module.exports = (sequelize, DataTypes) => {
    return sequelize.define('commandResponse', {
        response: DataTypes.TEXT
    });
}