module.exports = (sequelize, DataTypes) => {
    return sequelize.define('bankItem', {
        cost: DataTypes.INTEGER,
        name: DataTypes.STRING,
        description: DataTypes.TEXT,
        type: DataTypes.INTEGER,
        dataAttributes: DataTypes.STRING
    });
}