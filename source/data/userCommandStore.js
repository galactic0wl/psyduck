const _ = require('lodash');
const moment = require('moment');
const Sequalize = require('sequelize');
const Op = Sequalize.Op;
const { Command, CommandCode, CommandResponse } = require('./dbObjects');

const Debug = require('../utilities/debug');
/**
 * 
 * Used to perform operations on user command related models.
 * 
 * @class UserCommandStore
 */
class UserCommandStore {
    constructor() {
    }
    /**
     * 
     * 
     * @param {Array} codes - A list of codes for the new command.
     * @param {Array} responses - A list of responses for the new command.
     * @param {string} creator - The originating user who created the command.
     * @memberof UserCommandStore
     */
    async addCommand(codes, responses, creator) {
        const existingCommand = await this.findCommand(...codes);
        if (existingCommand) {
            Debug.logError(`A command with codes including ${codes} already exists. Modify this command instead.`);
        } else {
            await Command.create({
                addedBy: creator,
                codes: codes.map(c => { return { code: c }; }),
                responses: responses.map(r => { return { response: r }; })
            }, {
                    include: [{
                        model: CommandCode,
                        as: 'codes'
                    }, {
                        model: CommandResponse,
                        as: 'responses'
                    }]
                });
        }
    }
    /**
     * 
     * Used to convert a list of code strings to a list of model objects.
     * 
     * @param {Array} codes - A list of command code strings.
     * @returns {Array} - List of commandCodes Sequelize models.
     * @memberof UserCommandStore
     */
    async buildCommandCodes(...codes) {
        let newCodes = [];
        for (const c of codes) {
            const commandCode = await CommandCode.create({ code: c });
            newCodes.push(commandCode);
        }
        return newCodes;
    }
    /**
     * 
     * Used to convert a list of response strings to a list of model objects.
     * 
     * @param {Array} responses - A list of response strings.
     * @returns {Array} - List of commandResponses Sequelize models.
     * @memberof UserCommandStore
     */
    async buildCommandResponses(...responses) {
        let newResponses = [];
        for (const r of responses) {
            const commandResponse = await CommandResponse.create({ response: r });
            newResponses.push(commandResponse);
        }
        return newResponses;
    }
    /**
     * 
     * Update an existing command with new codes or responses.
     * 
     * @param {string} commandCode - A code that will match any of the existing codes for the command you wish to update.
     * @param {object} options - An object consisting of a list of codes and a list of responses.
     * @param {Array} options.codes - A list of code strings.
     * @param {Array} options.responses - A list of response strings.
     * @memberof UserCommandStore
     * @function
     */
    async updateCommand(commandCode, options) {
        const existingCommand = await this.findCommand(commandCode);
        if (existingCommand) {
            const commandCodes = await this.buildCommandCodes(..._.difference(options.codes, existingCommand.codes.map(c => c.code)));
            const commandResponses = await this.buildCommandResponses(..._.difference(options.responses, existingCommand.responses.map(r => r.response)));
            try {
                await existingCommand.addCodes([...commandCodes]);
                await existingCommand.addResponses([...commandResponses]);
            } catch (error) {
                Debug.logError(`Error: ${error.message}`);
            }
        } else {
            Debug.logError(`No command with codes including ${codes} exists. Add this command instead.`);
        }
    }
    /**
     * 
     * Attempts to find and return a command matching the supplied command codes from the database.
     * 
     * @param {Array} commandCodes - A list of command code strings to match.
     * @returns {Model} - Sequelize Model.
     * @memberof UserCommandStore
     */
    async findCommand(...commandCodes) {
        return await Command.findOne({
            include: [{
                model: CommandCode,
                where: { code: { [Op.or]: [...commandCodes] } },
                as: 'codes'
            }, {
                model: CommandResponse,
                as: 'responses'
            }]
        });
    }
}

module.exports = UserCommandStore;