const _ = require('lodash');
const moment = require('moment');
const config = require('../../config.json');
const Debug = require('../../utilities/debug');

const successResponses = [
    (userId) => `Well I'll be damned <@${userId}>. Ya might want to tighten those lips.`,
    (userId) => `Here we go again <@${userId}>, disrespecting your family...`,
    (userId) => `You uh.. might want to get a lawyer <@${userId}>`
];

const ignorableCommands = ["queue", "stop", "skip", "additem"];

/**
 * 
 * 
 * @class UserQuotes
 */
class UserQuotes {
    /**
     * 
     * 
     * @todo Extract this to a global utility.
     * @todo Handle command arguments more elegantly.
     * 
     * @static
     * @param {string} prefix - Command prefix as set in config.js
     * @param {string} messageContent - Raw content from message.
     * @returns {CommandParts} commandParts - An object containing the raw command and a list of arguments passed.
     * 
     * @memberof UserQuotes
     */
    static commandParts(prefix, messageContent) {
        const commandArgs = messageContent.slice(prefix.length).split(' ');
        const command = commandArgs.shift();
        return { code: command, args: [...commandArgs] };
    }
    /**
     * 
     * Method to check if a command exists. If a matching command is found, send one of its responses to the channel.
     * 
     * @static
     * @param {object} message - Message received from Discord.
     * @param {RootStore} rootStore - Root data store containing members of all substores.
     * @returns {function} - Function returned to pass in next function from the middleware stack.
     * @memberof UserQuotes
     */
    static command(message, rootStore) {
        return function (next) {
            Debug.log(`Begin handle command`);
            const { userQuotes } = rootStore;
            if (message.author.bot) {
                Debug.logWarning('Message is from bot user, ignoring.');
                next(message, rootStore);
                return;
            }
            if (message.content.startsWith(config.commandPrefix)) {
                Debug.log(`Message starts with command prefix...`);
                Debug.log(`Processing command...`);
                const rawCommand = UserQuotes.commandParts(config.commandPrefix, message.content);
                Debug.logInfo(`[Quotes] Command code: ${rawCommand.code}`);
                if (rawCommand.args.length > 0) { Debug.logInfo(`[Quotes] Command args: ${rawCommand.args}`); }

                if (_.indexOf(ignorableCommands, rawCommand.code) > -1) {
                    Debug.logWarning(`Command with code [${rawCommand.code}] is ignorable; skipping.`);
                    next(message, rootStore);
                    return;
                }

                var [
                    userId
                ] = rawCommand.args;

                if (rawCommand.code === 'quote') {
                    UserQuotes.grabQuote(message, userId, userQuotes);
                } else if (rawCommand.code === 'random') {
                    UserQuotes.getQuote(message, userId, userQuotes);
                }

            } else {
                Debug.logError(`Message does not start with command prefix ${config.commandPrefix}`);
                Debug.log(`End handle command`);
            }
            next(message, rootStore);
        }
    }
    static async getQuote(message, userId, userQuotes) {

        if (userId.startsWith('<@')) {
            userId = userId.match(/\<@(.*?)\>/)[1];
            userId = userId.replace('!', '');
        }

        const [user] = await userQuotes.findUser(userId);
        if (user.quotes && user.quotes.length > 0) {
            const quote = _.sample(user.quotes);
            const createdAt = moment(quote.createdAt);
            message.channel.send(`Remember this one <@${userId}>? "${quote.content}" - Snagged by <@${quote.capturerId}> at ${createdAt.format('ddd MMM DD YYYY h:mm A')}`);
        } else {
            message.channel.send(`Nope... nah, I got nothin.`);
        }
    }
    static async grabQuote(message, userId, userQuotes) {

        if (userId.startsWith('<@')) {
            userId = userId.match(/\<@(.*?)\>/)[1];
            userId = userId.replace('!', '');
        }

        const user = await message.guild.fetchMember(userId);
        const messageMap = await message.channel.fetchMessages({ limit: 100 });
        const messages = [...messageMap.values()];
        const lastMessage = _.find(messages, (m) => m.author.id === userId);

        userQuotes.addQuote(lastMessage.content, userId, message.author.id).then(result => {
            if (result) {
                message.channel.send(_.sample(successResponses)(userId));
            } else {
                message.channel.send(`Oops, maybe put a little more hand on it next time. I couldn't grab that one!`);
            }
        });
    }
}

module.exports = UserQuotes.command;