const _ = require('lodash');
const config = require('../../config.json');
const Debug = require('../../utilities/debug');

const ignorableCommands = ["queue", "stop", "skip", "quote", "random", "additem", "balance", "join"];

/**
 * 
 * 
 * @class UserCommands
 */
class UserCommands {
    /**
     * 
     * 
     * @todo Extract this to a global utility.
     * @todo Handle command arguments more elegantly.
     * 
     * @static
     * @param {string} prefix - Command prefix as set in config.js
     * @param {string} messageContent - Raw content from message.
     * @returns {CommandParts} commandParts - An object containing the raw command and a list of arguments passed.
     * 
     * @memberof UserCommands
     */
    static commandParts(prefix, messageContent) {
        const commandArgs = messageContent.slice(prefix.length).split(' ');
        const command = commandArgs.shift();
        return { code: command, args: [...commandArgs] };
    }
    /**
     * 
     * Method to check if a command exists. If a matching command is found, send one of its responses to the channel.
     * 
     * @static
     * @param {object} message - Message received from Discord.
     * @param {RootStore} rootStore - Root data store containing members of all substores.
     * @returns {function} - Function returned to pass in next function from the middleware stack.
     * @memberof UserCommands
     */
    static command(message, rootStore) {
        return function (next) {
            Debug.log(`Begin handle command`);
            const { userCommands } = rootStore;
            if (message.author.bot) {
                Debug.logWarning('Message is from bot user, ignoring.');
                next(message, rootStore);
                return;
            }
            if (message.content.startsWith(config.commandPrefix)) {
                Debug.log(`Message starts with command prefix...`);
                Debug.log(`Processing command...`);
                const rawCommand = UserCommands.commandParts(config.commandPrefix, message.content);
                Debug.logInfo(`[Commands] Command code: ${rawCommand.code}`);
                if (rawCommand.args.length > 0) { Debug.logInfo(`[Commands] Command args: ${rawCommand.args}`); }

                if (_.indexOf(ignorableCommands, rawCommand.code) > -1) {
                    Debug.logWarning(`Command with code [${rawCommand.code}] is ignorable; skipping.`);
                    next(message, rootStore);
                    return;
                }

                Debug.log(`Searching for command in data store...`);
                userCommands.findCommand(rawCommand.code).then(command => {
                    if (command) {
                        Debug.log(`Command found!`);
                        const randomResponse = _.sample(command.responses).response;
                        Debug.logInfo(`Responding with random item ${randomResponse}`);
                        message.channel.send(randomResponse);
                    } else {
                        Debug.logWarning(`No commands matching [${rawCommand.code}] found in data store.`);
                        message.channel.send(`RIP you <@${message.author.id}>! You know why? Because I don't have that command you dolt.`);
                    }
                }).catch(error => {
                    Debug.logError(error);
                });
            } else {
                Debug.logError(`Message does not start with command prefix ${config.commandPrefix}`);
                Debug.log(`End handle command`);
            }
            next(message, rootStore);
        }
    }
}

module.exports = UserCommands.command;