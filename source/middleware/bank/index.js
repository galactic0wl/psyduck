const _ = require('lodash');
const config = require('../../config.json');
const Debug = require('../../utilities/debug');

class Bank {
    constructor(discordClient) {
        this.client = discordClient;
        this.client.on('message', message => this.handleRequest(message));
    }
    /**
 * 
 * @todo Extract this to a global utility.
 * @todo Handle command arguments more elegantly.
 * 
 * @static
 * @param {string} prefix - Command prefix as set in config.js
 * @param {string} messageContent - Raw content from message.
 * @returns {CommandParts} commandParts - An object containing the raw command and a list of arguments passed.
 * 
 * @memberof Bank
 */
    commandParts(prefix, messageContent) {
        // First split of raw message. Trim prefix, split on first space to get command code and all arguments.
        let commandArgs = messageContent.slice(prefix.length).split(/\s(.+)/);
        // Extract command code from array.
        const command = commandArgs.shift();
        // Return object for consumption.
        var queueList = [];
        if (commandArgs[0]) {
            queueList = commandArgs[0].split(',');
        }
        return { code: command, args: queueList };
    }
    /**
     * 
     * Method to route a bank command.
     * 
     * @param {object} message - Message from Discord
     * @memberof Bank
     */
    handleRequest(message) {
        if (message.author.bot) {
            Debug.logWarning('Message is from bot user, ignoring.');
            return;
        }
        if (message.content.startsWith(config.commandPrefix)) {
            Debug.log(`Message starts with command prefix...`);
            Debug.log(`Processing command...`);
            const rawCommand = this.commandParts(config.commandPrefix, message.content);
            Debug.logInfo(`[Bank] Command code: ${rawCommand.code}`);
            if (rawCommand.args.length > 0) { Debug.logInfo(`[Bank] Command args: ${rawCommand.args}`); }

            switch (rawCommand.code) {
                case 'balance':
                    this.checkBalance(message, rawCommand);
                    break;
            }
        }
    }
    async checkBalance(message, rawCommand) {
        const { bankStore } = this.rootStore;
        const [ user ] = await bankStore.findBank(message.author.id);
        let userBank = user.bank;
        if(!userBank) {
            const newBank = await bankStore.createBank();
            userBank = await user.setBank(newBank);
        }
        message.channel.send(`Your current balance is **${userBank.balance}** DuckyCoins`);
    }
}

module.exports = Bank;