const moment = require('moment');
const Debug = require('../../utilities/debug');

/**
 * 
 * Class used to route CRUD operations to the UserCommandStore.
 * 
 * @class RankManagement
 */
class RankManagement {

    static async assignRankAll(discordMessage, rootStore, options) {
        const [rank] = options;

        const { userStore, rankStore } = rootStore;
        const newRank = await rankStore.findRank(rank);
        if (!newRank) {
            discordMessage.channel.send(`Unable to find rank matching ${rank}.`);
        } else {
            const serverRole = discordMessage.guild.roles.find('name', newRank.associatedRole);
            const requiredRole = discordMessage.guild.roles.find('name', 'Authenticated User');
            if (serverRole) {

                discordMessage.guild.members.forEach((member, userId) => {
                    if (member.roles.get(requiredRole.id)) {
                        RankManagement.assignRankToUser(userId, newRank, userStore);
                        member.addRole(serverRole);
                    }
                });

                discordMessage.channel.send(`Added rank ${rank} to everyone.`);

            }

        }
    }
    static async assignRankToUser(userId, rank, userStore) {
        let [user] = await userStore.findUser(userId);
        await user.addRank(rank);
    }
    static async assignRank(discordMessage, rootStore, options) {
        const [rank] = options;
        let [userId] = options[1];
        if (userId.startsWith('<@')) {
            userId = userId.match(/\<@(.*?)\>/)[1];
            userId = userId.replace('!', '');
        }

        const { userStore, rankStore } = rootStore;
        let [user] = await userStore.findUser(userId);
        const newRank = await rankStore.findRank(rank);
        if (!newRank) {
            discordMessage.channel.send(`Unable to find rank matching ${rank}.`);
        } else {
            const serverRole = discordMessage.guild.roles.find('name', newRank.associatedRole);
            if (serverRole) {
                const guildUser = await discordMessage.guild.fetchMember(userId);
                if (guildUser) {
                    await user.addRank(newRank);
                    guildUser.addRole(serverRole);
                    discordMessage.channel.send(`Added rank ${rank} to user <@${userId}>.`);
                }

            }

        }
    }
    static async addRank(discordMessage, rootStore, options) {
        const {
            rankStore
        } = rootStore;

        const [
            rankName,
            rankData
        ] = options;

        const rankAttributes = JSON.parse(rankData);
        const { description, requiredExp, associatedRole, roleColor } = rankAttributes;

        try {
            const rank = await rankStore.addRank(rankName, description, requiredExp, associatedRole);
            if (rank) {
                await discordMessage.guild.createRole({ name: associatedRole, color: roleColor || 'WHITE' });
            }
            discordMessage.channel.send('Added rank to system.');
        } catch (error) {
            Debug.logError(error);
        }
    }
}

module.exports = RankManagement;